<?php
/**
 * WordPress基础配置文件。
 *
 * 这个文件被安装程序用于自动生成wp-config.php配置文件，
 * 您可以不使用网站，您需要手动复制这个文件，
 * 并重命名为“wp-config.php”，然后填入相关信息。
 *
 * 本文件包含以下配置选项：
 *
 * * MySQL设置
 * * 密钥
 * * 数据库表名前缀
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/zh-cn:%E7%BC%96%E8%BE%91_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL 设置 - 具体信息来自您正在使用的主机 ** //
/** WordPress数据库的名称 */
define('DB_NAME', 'eyee');

/** MySQL数据库用户名 */
define('DB_USER', 'root');

/** MySQL数据库密码 */
define('DB_PASSWORD', '123456');

/** MySQL主机 */
define('DB_HOST', '127.0.0.1');

/** 创建数据表时默认的文字编码 */
define('DB_CHARSET', 'utf8mb4');

/** 数据库整理类型。如不确定请勿更改 */
define('DB_COLLATE', '');

/**#@+
 * 身份认证密钥与盐。
 *
 * 修改为任意独一无二的字串！
 * 或者直接访问{@link https://api.wordpress.org/secret-key/1.1/salt/
 * WordPress.org密钥生成服务}
 * 任何修改都会导致所有cookies失效，所有用户将必须重新登录。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'up8_#iBW)=E7Yo?DoLuPB>m<n%3&$Mo=qN8!~.VBCkeEXGEgtOe8ffY-xJJW#cxg');
define('SECURE_AUTH_KEY',  '@t6mftg.W=9V1t%s@FE9yWr-^oVt?ALll:Be.5~k]~+X;$ojcYbIah(@1mE=6O!1');
define('LOGGED_IN_KEY',    'c_Ru4>/8+-/(s*V]n>9z:/<{$k|Wd@wsJU}@UcS&&B*EpaS_U6s&7G`XsaM+$[Mk');
define('NONCE_KEY',        'PF`l_*2CZAnv&EU%J?Z^m]-2+%qu=d]{[h,Z#bUelF(tLm*#m6NkhdH_9:vDM_5J');
define('AUTH_SALT',        'Gw2Qx&HAQ2i6wv>^wcc <9!AKgHVjP,q|:q]) 3Cg~0!6%b~us:nbi~71NTreDR#');
define('SECURE_AUTH_SALT', ')iLI;WI~!0Q|X9ds^M@ZP^|cj|`}xsY$w@qjchZwl?tL37xK.yI:!n(/Z?s$d1|K');
define('LOGGED_IN_SALT',   'u$8+Y(/HvA-uL*::*(BM@vS{,4n[:{OAg[BQ~E&(Mt1V&?RmO08%wRVHWj2Q5MUS');
define('NONCE_SALT',       'uEhFP4h4c`^)i90~cXm >kDbn=rtcC4+B-QPvC%z&o#G4in2F}bVOW.gm^yQo0`4');

/**#@-*/

/**
 * WordPress数据表前缀。
 *
 * 如果您有在同一数据库内安装多个WordPress的需求，请为每个WordPress设置
 * 不同的数据表前缀。前缀名只能为数字、字母加下划线。
 */
$table_prefix  = 'wp_';

/**
 * 开发者专用：WordPress调试模式。
 *
 * 将这个值改为true，WordPress将显示所有用于开发的提示。
 * 强烈建议插件开发者在开发环境中启用WP_DEBUG。
 *
 * 要获取其他能用于调试的信息，请访问Codex。
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/**
 * zh_CN本地化设置：启用ICP备案号显示
 *
 * 可在设置→常规中修改。
 * 如需禁用，请移除或注释掉本行。
 */
define('WP_ZH_CN_ICP_NUM', true);

/* 好了！请不要再继续编辑。请保存本文件。使用愉快！ */

/** WordPress目录的绝对路径。 */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** 设置WordPress变量和包含文件。 */
require_once(ABSPATH . 'wp-settings.php');
